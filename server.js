const express = require("express");
const cors = require("cors");
const app = express();
const path = require('path');
const utilisateurRoute = require("./Route/clientRoute.js");
const commentaireRoute = require("./Route/commentaireRoute.js");
const technologieRoutes = require("./Route/technologieRoute.js");

app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/utilisateur', utilisateurRoute);
app.use('/commentaires', commentaireRoute);
app.use('/technologie', technologieRoutes);

app.listen(8000);
