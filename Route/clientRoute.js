const express = require("express");
const router = express.Router();
const clientController = require("../Controller/clientController.js");
const middleware = require("../Middleware/authentification.js");

router.get("/getAll", middleware.authentification, middleware.verifyAdmin, clientController.getAllClient);
router.post('/add', clientController.addUser);
router.post('/login', clientController.loginUser);
router.get('/:id', clientController.getUserById);
router.put('/modify/:id', clientController.modifyUser);
router.delete('/delete/:id', clientController.deleteUser);

module.exports = router;