const express = require('express');
const router = express.Router();
const technologieController = require('../Controller/technologieController');
const middleware = require("../Middleware/authentification.js");

router.post('/add', middleware.authentification, middleware.verifyAdmin, technologieController.createTechnologie);
router.get('/getAll', technologieController.getAllTechnologies);
router.get('/:id', technologieController.getTechnologie);
router.put('/modify/:id', middleware.authentification, middleware.verifyAdmin, technologieController.updateTechnologie);
router.delete('/delete/:id', middleware.authentification, middleware.verifyAdmin, technologieController.deleteTechnologie);

module.exports = router;
