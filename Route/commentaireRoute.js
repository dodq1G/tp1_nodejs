const express = require("express");
const router = express.Router();
const commentaireController = require("../Controller/commentaireController.js");
const middleware = require("../Middleware/authentification.js");

router.get('/getAll', middleware.authentification, commentaireController.getAllCommentaire);
router.get('/technologie/:id', middleware.authentification, commentaireController.getAllCommentaireByTechno);
router.post('/add', middleware.authentification, middleware.verifyJournaliste, commentaireController.addCommentaire);
router.get('/avant/:date', middleware.authentification, commentaireController.beforeCommentaire);

module.exports = router;