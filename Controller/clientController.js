const pool = require("../database.js");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

async function getAllClient(req, res) {
    let conn;
    try {
        conn = await pool.getConnection();
        const rows = await conn.query('SELECT * FROM utilisateur');
        res.status(200).json(rows);
    } catch (error) {
        res.status(500).json({ error: error.message });
    } finally {
        if (conn) conn.release();
    }
}

async function addUser(req, res) {
    try {
        const { nom, prenom, email, motdepasse } = req.body;
        const hashedPassword = await bcrypt.hash(motdepasse, 10);
  
        const verify = await pool.query("SELECT * FROM utilisateur WHERE email = ?", [email]);
        if(verify.length > 0){
          return res.status(401).json({error : "Utilisateur déjà existant"});
        }
  
        const result = await pool.query('INSERT INTO utilisateur (nom, prenom, email, mdp, role) VALUES (?, ?, ?, ?, ?)', [nom, prenom, email, hashedPassword, "user"]);
  
        const token = jwt.sign({email}, process.env.SECRET_KEY, {expiresIn : "1h"});
  
        res.status(201).json({token});
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

async function loginUser(req, res) {
    const { email, motdepasse } = req.body;
    const result = await pool.query("SELECT * FROM utilisateur WHERE email = ?", [email]);
    if(result.length == 0){
      return res.status(401).json({error : "Utilisateur déjà existant"});
    }
    const client = result[0];
    await bcrypt.compare(motdepasse, client.mdp, function(err, result){
      if (result) {
        const token = jwt.sign({email}, process.env.SECRET_KEY, {expiresIn : "1h"});
        res.status(201).json({token});
      } else {
        return res.status(401).json({error : "mot de passe incorrect"});
      }
    });
}

async function getUserById(req, res) {
    const id = req.params.id;
    let conn;
    try {
        conn = await pool.getConnection();
        const results = await conn.query('SELECT * FROM utilisateur WHERE id = ?', [id]);
        if (results.length === 0) {
            return res.status(404).json({ message: 'Utilisateur non trouvé' });
        }
        res.status(200).json(results[0]);
    } catch (error) {
        res.status(500).json({ error: error.message });
    } finally {
        if (conn) conn.release();
    }
}

async function modifyUser(req, res) {
    const id = req.params.id;
    const { nom, prenom, email } = req.body;
    let conn;
    try {
        conn = await pool.getConnection();
        const results = await conn.query('UPDATE utilisateur SET nom = ?, prenom = ?, email = ? WHERE id = ?', [nom, prenom, email, id]);
        if (results.affectedRows === 0) {
            return res.status(404).json({ message: 'Utilisateur non trouvé' });
        }
        res.status(200).json({ message: 'Utilisateur mis à jour' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    } finally {
        if (conn) conn.release();
    }
}

async function deleteUser(req, res) {
    const id = req.params.id;
    let conn;
    try {
        conn = await pool.getConnection();
        const results = await conn.query('DELETE FROM utilisateur WHERE id = ?', [id]);
        if (results.affectedRows === 0) {
            return res.status(404).json({ message: 'Utilisateur non trouvé' });
        }
        res.status(200).json({ message: 'Utilisateur supprimé' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    } finally {
        if (conn) conn.release();
    }
}

module.exports = {
    getAllClient,
    modifyUser,
    loginUser,
    getUserById,
    addUser,
    deleteUser
};
