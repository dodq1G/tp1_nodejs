const pool = require('../database.js');

async function createTechnologie(req, res) {
    const { nom_techno, nom_createur, date_creation } = req.body;
    try {
        const result = await pool.query('INSERT INTO technologie (nom_techno, nom_createur, date_creation) VALUES (?, ?, ?)', [nom_techno, nom_createur, date_creation]);
        res.status(200).json({ message: 'Technologie mise à jour' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

async function getAllTechnologies(req, res) {
    try {
        const rows = await pool.query('SELECT * FROM technologie');
        res.status(200).json(rows);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

async function getTechnologie(req, res) {
    const { id } = req.params;
    try {
        const rows = await pool.query('SELECT * FROM technologie WHERE id = ?', [id]);
        if (rows.length === 0) {
            return res.status(404).json({ message: 'Technologie non trouvée' });
        }
        res.status(200).json(rows[0]);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

async function updateTechnologie(req, res) {
    const { id } = req.params;
    const {nom_techno, nom_createur, date_creation } = req.body;
    try {
        const result = await pool.query('UPDATE technologie SET nom_techno = ?, nom_createur = ?, date_creation = ? WHERE id = ?', [nom_techno, nom_createur, date_creation, id]);
        if (result.affectedRows === 0) {
            return res.status(404).json({ message: 'Technologie non trouvée' });
        }
        res.status(200).json({ message: 'Technologie mise à jour' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

async function deleteTechnologie(req, res) {
    const { id } = req.params;
    try {
        const result = await pool.query('DELETE FROM technologie WHERE id = ?', [id]);
        if (result.affectedRows === 0) {
            return res.status(404).json({ message: 'Technologie non trouvée' });
        }
        res.status(200).json({ message: 'Technologie supprimée' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

module.exports = {
    createTechnologie,
    getAllTechnologies,
    getTechnologie,
    updateTechnologie,
    deleteTechnologie
};