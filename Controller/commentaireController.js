const pool = require("../database.js");

async function getAllCommentaire(req, res) {
    let conn;
    try {
        conn = await pool.getConnection();
        const results = await conn.query('SELECT * FROM commentaire');
        res.status(200).json(results);
    } catch (error) {
        res.status(500).json({ error: error.message });
    } finally {
        if (conn) conn.release();
    }
}

async function getAllCommentaireByTechno(req, res) {
    const technologieId = req.params.id;
    let conn;
    try {
        conn = await pool.getConnection();
        const results = await conn.query('SELECT * FROM commentaire WHERE technologie_id = ?', [technologieId]);
        res.status(200).json(results);
    } catch (error) {
        res.status(500).json({ error: error.message });
    } finally {
        if (conn) conn.release();
    }
}

async function addCommentaire(req, res) {
    const { message, utilisateur_id, technologie_id } = req.body;
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0'); // Les mois commencent à 0
    const day = String(now.getDate()).padStart(2, '0');
    const date = `${year}-${month}-${day}`;

    try {
        const result = await pool.query('INSERT INTO commentaire (message, date_creation_commentaire, utilisateur_id, technologie_id) VALUES (?, ?, ?, ?)', [message, date, utilisateur_id, technologie_id]);
        res.status(201).json({ message : "Commentaire ajouté avec succès" });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

async function beforeCommentaire(req, res) {
    const date = req.params.date;
    let conn;
    try {
        conn = await pool.getConnection();
        const results = await conn.query('SELECT * FROM commentaire WHERE date_creation_commentaire < ?', [date]);
        res.status(200).json(results);
    } catch (error) {
        res.status(500).json({ error: error.message });
    } finally {
        if (conn) conn.release();
    }
}

module.exports = {
    beforeCommentaire,
    getAllCommentaireByTechno,
    getAllCommentaire,
    addCommentaire
};

