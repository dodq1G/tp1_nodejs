const jwt = require("jsonwebtoken");
const pool = require("../database.js");
require("dotenv").config();

exports.authentification = (req, res, next) => {
    const token = req.params.token ? req.params.token : req.headers.authorization;
    if(token && process.env.SECRET_KEY){
        jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
            if(err){
                res.status(401).json({error : "Accès refusé"});
            }else{
                next();
            }
        })
    }else{
        res.status(401).json({error : "Accès refusé"});
    }
}

exports.verifyAdmin = async (req, res, next) => {
    try {
        const tokenHeader = req.headers.authorization;
        if (!tokenHeader) return res.status(401).json({ message: 'Token non fourni' });

        const token = tokenHeader
        const decoded = jwt.verify(token, process.env.SECRET_KEY);

        const email = decoded.email;
        const query = 'SELECT role FROM utilisateur WHERE email = ?';
        const users = await pool.query(query, [email]);

        if (users.length === 0 || users[0].role !== 'admin') {
            return res.status(401).json({ message: 'Accès refusé' });
        }

        next();
    } catch (error) {
        console.error('Erreur de vérification :', error);
        res.status(401).json({ message: 'Non autorisé' });
    }
};

exports.verifyJournaliste = async (req, res, next) => {
    try {
        const tokenHeader = req.headers.authorization;
        if (!tokenHeader) return res.status(401).json({ message: 'Token non fourni' });

        const token = tokenHeader
        const decoded = jwt.verify(token, process.env.SECRET_KEY);

        const email = decoded.email;
        const query = 'SELECT role FROM utilisateur WHERE email = ?';
        const users = await pool.query(query, [email]);

        if (users.length === 0 || users[0].role !== 'journaliste' && users[0].role !== 'admin') {
            return res.status(401).json({ message: 'Accès refusé' });
        }

        next();
    } catch (error) {
        console.error('Erreur de vérification :', error);
        res.status(401).json({ message: 'Non autorisé' });
    }
};

